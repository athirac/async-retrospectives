#!/usr/bin/env ruby

require 'docopt'
require 'yaml'

require './lib/gitlab_api'
require './lib/retrospective'
require './lib/team'

docstring = <<DOCSTRING
Create a retrospective issue

Usage:
  #{__FILE__} (create|update) --token=<token> (--all-teams|--team=<team>) [--dry-run]
  #{__FILE__} -h | --help

Options:
  -h --help        Show this screen.
  --dry-run        Print the issue contents to standard output.
  --token=<token>  GitLab API token.
  --all-teams      Run for all teams and print a summary of the results.
  --team=<team>    Team name, including initial caps (team info in teams.yml).
DOCSTRING

def run_for_all_teams
  results = Team.all.map do |team|
    success =
      begin
        yield team

        true
      rescue => e
        puts "#{team.name} failed:"
        puts e
        puts e.backtrace

        false
      end

    [team.name, success]
  end

  exit_code = results.all?(&:last) ? 0 : 1

  puts Retrospective::LOG_SEPARATOR
  puts 'All done!'

  results.each do |(name, success)|
    puts "#{name} #{success ? 'succeeded' : 'failed'}"
  end

  puts Retrospective::LOG_SEPARATOR
  puts ''

  exit exit_code
end

begin
  options = Docopt::docopt(docstring)
  retrospective = Retrospective.new(options.fetch('--token'))
  dry_run = options.fetch('--dry-run', false)

  run = lambda do |team|
    if options.fetch('create', false)
      retrospective.create_issue(team: team, dry_run: dry_run)
    elsif options.fetch('update', false)
      retrospective.update_issue(team: team, dry_run: dry_run)
    end
  end

  if options.fetch('--all-teams', false)
    run_for_all_teams(&run)
  else
    run.call(Team.find(options.fetch('--team')))
  end
rescue Docopt::Exit => e
  puts e.message
  exit 1
rescue GitlabApi::UpdateFailed
  exit 1
end
